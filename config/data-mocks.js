const moment = require('moment');

class dataMock {
  static getCompanyById(companyId) {
    return {
      id: 1,
      companyId: 'demo@osf-global-demo.com',
      name: 'OSF Glasses',
      description: 'A demo company for selling glasses',
      active: true
    };
  }

  static getStoresByCompanyId(companyId) {
    return [
      {
        id: 1,
        companyId: 1,
        name: 'OSF Glasses Fortaleza',
        street: 'Rua Dr. Gilberto Studart, 55, Cocó',
        complement: 'Torre Norte, Mezanino',
        country: 'Brazil',
        city: 'Fortaleza',
        state: 'Ceará',
        lat: -3.741378,
        lng: -38.487372,
        active: true
      },
      {
        id: 2,
        companyId: 1,
        name: 'OSF Glasses Rio',
        street: 'Rua Humaita, 85, Humaita',
        complement: '8th Floor',
        country: 'Brazil',
        city: 'Rio de Janeiro',
        state: 'Rio de Janeiro',
        lat: -22.955949,
        lng: -43.197932,
        active: true
      }
    ]
  }

  static getAppointmentTypesByStoreId(companyId, storeId) {
    return [
      {
        id: 1,
        companyId: 1,
        name: 'Item reservation',
        description: 'Appointment to meet the seller to retrieve a reserved item',
        active: true,
        type: 'RESERVATION'
      },
      {
        id: 2,
        companyId: 1,
        name: 'Return Item',
        description: 'Appointment to return some damaged or unwanted goods',
        active: true,
        type: 'RETURN'
      },
      {
        id: 3,
        companyId: 1,
        name: 'Take Eye Prescription',
        description: 'Appointment to take eye prescription with a professinal',
        active: true,
        type: 'EYE_PRESCRIPTION'
      }
    ];
  }

  static getSlotsByStoreId(companyId, storeId, appointmentTypeId) {
    if(appointmentTypeId) {
      switch (appointmentTypeId) {
        case '1': 
          return [
            {
              id: 2,
              storeId: 1,
              name: 'Product Reservation 01',
              peopleQty: 1,
              timeslice: 30,
              active: true,
              opens: moment('2018-01-01T08:00:00.000Z'),
              closes: moment('2018-01-01T21:00:00.000Z')
            },
            {
              id: 3,
              storeId: 1,
              name: 'Product Reservation 02',
              peopleQty: 1,
              timeslice: 30,
              active: true,
              opens: moment('2018-01-01T08:00:00.000Z'),
              closes: moment('2018-01-01T21:00:00.000Z')
            }
          ];
        case '2':
          return [
            {
              id: 2,
              storeId: 1,
              name: 'Product Reservation 01',
              peopleQty: 1,
              timeslice: 30,
              active: true,
              opens: moment('2018-01-01T08:00:00.000Z'),
              closes: moment('2018-01-01T21:00:00.000Z')
            },
            {
              id: 3,
              storeId: 1,
              name: 'Product Reservation 02',
              peopleQty: 1,
              timeslice: 30,
              active: true,
              opens: moment('2018-01-01T08:00:00.000Z'),
              closes: moment('2018-01-01T21:00:00.000Z')
            }
          ];
        case '3':
          return [
            {
              id: 1,
              storeId: 1,
              name: 'Prescription Room',
              peopleQty: 1,
              timeslice: 30,
              active: true,
              opens: moment('2018-01-01T10:00:00.000Z'),
              closes: moment('2018-01-01T16:00:00.000Z')
            }
          ];
        default: 
          return []
      }
    }
    return [
      {
        id: 1,
        storeId: 1,
        name: 'Prescription Room',
        peopleQty: 1,
        timeslice: 30,
        active: true,
        opens: moment('2018-01-01T10:00:00.000Z'),
        closes: moment('2018-01-01T16:00:00.000Z')
      },
      {
        id: 2,
        storeId: 1,
        name: 'Product Reservation 01',
        peopleQty: 1,
        timeslice: 30,
        active: true,
        opens: moment('2018-01-01T08:00:00.000Z'),
        closes: moment('2018-01-01T21:00:00.000Z')
      },
      {
        id: 3,
        storeId: 1,
        name: 'Product Reservation 02',
        peopleQty: 1,
        timeslice: 30,
        active: true,
        opens: moment('2018-01-01T08:00:00.000Z'),
        closes: moment('2018-01-01T21:00:00.000Z')
      }
    ];
  }

  static getEmployeesByStoreId(companyId, storeId, appointmentTypeId) {
    if(appointmentTypeId) {
      switch(appointmentTypeId) {
        case '1': 
          return [
            {
              id: 1,
              storeId: 1,
              firstName: 'Breno',
              middleName: 'Ribeiro',
              lastName: 'Monteiro',
              active: true,
              role: 2
            },
            {
              id: 2,
              storeId: 1,
              firstName: 'Daniel',
              middleName: 'Laurindo',
              lastName: 'Souza',
              active: true,
              role: 2
            }
          ];
        case '2': 
          return [
            {
              id: 1,
              storeId: 1,
              firstName: 'Breno',
              middleName: 'Ribeiro',
              lastName: 'Monteiro',
              active: true,
              role: 2
            },
            {
              id: 2,
              storeId: 1,
              firstName: 'Daniel',
              middleName: 'Laurindo',
              lastName: 'Souza',
              active: true,
              role: 2
            }
          ];
        case '3':
          return [
            {
              id: 3,
              storeId: 1,
              firstName: 'Clairton',
              middleName: 'Menezes',
              lastName: 'da Silva Junior',
              active: true,
              role: 1
            }
          ];
        default:
          return [];
      }
    }
    return [
      {
        id: 1,
        storeId: 1,
        firstName: 'Breno',
        middleName: 'Ribeiro',
        lastName: 'Monteiro',
        active: true,
        role: 2
      },
      {
        id: 2,
        storeId: 1,
        firstName: 'Daniel',
        middleName: 'Laurindo',
        lastName: 'Souza',
        active: true,
        role: 2
      },
      {
        id: 3,
        storeId: 1,
        firstName: 'Clairton',
        middleName: 'Menezes',
        lastName: 'da Silva Junior',
        active: true,
        role: 1
      }
    ];
  }

  static getCustomersByCompanyId(companyId) {
    return [
      {
        id: 1,
        companyId: 1,
        email: 'grace.smith@osf-commerce.com',
        firstName: 'Grace',
        middleName: '',
        lastName: 'Smith',
        phone: '1213987654876',
        gender: 'F',
        document: ''
      },
      {
        id: 2,
        companyId: 1,
        email: 'stefan.ioachim@osf-global.com',
        firstName: 'Stefan',
        middleName: '',
        lastName: 'Ioachim',
        phone: '',
        gender: 'M',
        document: ''
      },
      {
        id: 3,
        companyId: 1,
        email: 'amilcar.rodrigues@osf-commerce.com',
        firstName: 'Amílcar',
        middleName: '',
        lastName: 'Rodrigues',
        phone: '55859999999999',
        gender: 'M',
        document: ''
      },
      {
        id: 4,
        companyId: 1,
        email: 'marcus.reuber@osf-commerce.com',
        firstName: 'Marcus',
        middleName: '',
        lastName: 'Reuber',
        phone: '55859911111111',
        gender: 'M',
        document: ''
      }
    ]
  }

  static getAppointmentsByStoreId(companyId, storeId, params) {
    const { starts, ends, employee, slot } = params;
    const values = [
      {
        id: 1,
        appointmentId: 1,
        employeeId: 1,
        appointmentTypeId: 1,
        slotId: 2,
        customerId: 1,
        starts: moment('2019-04-03T08:00:00.000Z'),
        ends: moment('2019-04-03T08:30:00.000Z'),
        status: 'OPEN',
        description: 'Customer reserved a product',
        comments: '',
      },
      {
        id: 2,
        appointmentId: 2,
        employeeId: 2,
        appointmentTypeId: 2,
        slotId: 3,
        customerId: 2,
        starts: moment('2019-04-03T08:00:00.000Z'),
        ends: moment('2019-04-03T08:30:00.000Z'),
        status: 'OPEN',
        description: 'Customer wants to return a product',
        comments: '',
      },
      {
        id: 3,
        appointmentId: 1,
        employeeId: 1,
        appointmentTypeId: 1,
        slotId: 3,
        customerId: 1,
        starts: moment('2019-04-03T17:00:00.000Z'),
        ends: moment('2019-04-03T17:30:00.000Z'),
        status: 'OPEN',
        description: 'Customer reserved a product',
        comments: '',
      },
      {
        id: 4,
        appointmentId: 3,
        employeeId: 3,
        appointmentTypeId: 3,
        slotId: 1,
        customerId: 1,
        starts: moment('2019-04-03T13:00:00.000Z'),
        ends: moment('2019-04-03T13:30:00.000Z'),
        status: 'OPEN',
        description: 'Customer wants an eye precription',
        comments: '',
      },
      {
        id: 5,
        appointmentId: 5,
        employeeId: 1,
        appointmentTypeId: 2,
        slotId: 3,
        customerId: 3,
        starts: moment('2019-04-03T13:00:00.000Z'),
        ends: moment('2019-04-03T13:30:00.000Z'),
        status: 'OPEN',
        description: 'Customer wants to return a product',
        comments: '',
      },
      {
        id: 6,
        appointmentId: 6,
        employeeId: 3,
        appointmentTypeId: 3,
        slotId: 1,
        customerId: 1,
        starts: moment('2019-04-04T10:00:00.000Z'),
        ends: moment('2019-04-04T10:30:00.000Z'),
        status: 'OPEN',
        description: 'Customer wants an eye precription',
        comments: '',
      },
      {
        id: 7,
        appointmentId: 7,
        employeeId: 2,
        appointmentTypeId: 2,
        slotId: 2,
        customerId: 2,
        starts: moment('2019-04-04T10:00:00.000Z'),
        ends: moment('2019-04-04T10:30:00.000Z'),
        status: 'OPEN',
        description: 'Customer wants to return a product',
        comments: '',
      },
      {
        id: 8,
        appointmentId: 8,
        employeeId: 1,
        appointmentTypeId: 2,
        slotId: 3,
        customerId: 3,
        starts: moment('2019-04-04T10:00:00.000Z'),
        ends: moment('2019-04-04T10:30:00.000Z'),
        status: 'OPEN',
        description: 'Customer wants to return a product',
        comments: '',
      }
    ];
    if( employee || slot ) {
      values.filter( elem => {
        return (elem.starts.isSameOrAfter(moment(starts)) 
          && elem.ends.isSameOrBefore(moment(ends))) && (elem.employeeId.toString() === employee || 
          elem.slotId.toString() === slot); 
      })
    }
    return values.filter( elem => {
      return (elem.starts.isSameOrAfter(moment(starts)) && elem.ends.isSameOrBefore(moment(ends)))}
      );
  }
}

module.exports = dataMock;
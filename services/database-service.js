/* eslint-disable quotes */
const { Client } = require('pg');
const GenericHelper = require('../helpers/generic-helper');

/**
 * Instantiate the client object with the credentials
 * @param {JSON} config 
 * credentials example: 
 * {"user":"userExample", "host":"hostExample", "password":"passwordExample", "database":"databaseExample", "port":"portExample", "ssl":true}
 */
const connect = async (config) => {
    try {      
      const client = new Client(config);
      await client.connect();
      return client;
    } catch (error) {
      throw error;
    }
  };


class DatabaseService {     
 
  /**
   * The Joker
   * @param {JSON} credentials 
   * credentials example: 
   * {"user":"userExample", "host":"hostExample", "password":"passwordExample", "database":"databaseExample", "port":"portExample", "ssl":true}
   * @param {String} queryPrepared 
   */
  async executeQuery(credentials, queryPrepared) {
    const client = await connect(credentials);
    try {
      const res = await client.query(queryPrepared);
      return res;
    } catch (error) {
      throw error; 
    } finally {
      client.end();
    }
  };


/**
 * Insert Function 
 * @param {JSON} credentials 
 * credentials example: 
 * {"user":"userExample", "host":"hostExample", "password":"passwordExample", "database":"databaseExample", "port":"portExample", "ssl":true}
 * @param {String} table 
 * @param {Array<JSON>} clauses 
 */
  async insert(credentials, table, clauses) {
      const fields = Object.keys(clauses);
      const values = Object.values(clauses);
    try {
        const query = `INSERT INTO ${table}(${fields.reduce((elem, acc) => `${acc}, ${elem}`)}) VALUES(${values.reduce((elem, acc) => `${acc}, ${elem}`)})`;
        return await this.executeQuery(credentials, query);
    } catch (error) {
        throw error;
    } 
  }

 /**
  * Select with Where
  * @param {JSON} credentials 
  * credentials example: 
  * {"user":"userExample", "host":"hostExample", "password":"passwordExample", "database":"databaseExample", "port":"portExample", "ssl":true}
  * @param {String} table 
  * @param {Array<String>} fields 
  * @param {Array<JSON>} clauses clauses example: [{ 'id_company': '001'}, {'client_name': 'Zezinho'}]
  * @param {String} operator AND or OR 
  */
  async select(credentials, table, fields, clauses, operator) {
    const where = GenericHelper.transformJsonToFormatedString(clauses, operator);
    const query = `SELECT ${fields.reduce((elem, acc) => `${acc}, ${elem}`)} FROM ${table} WHERE ${where}`;
    return await this.executeQuery(credentials, query);
  }

  /*    
    Note that 'fields' shoud be in the same format of clauses, as the example below
        [{'name': 'changedName'}, {'price' : 20}]    
  */

  /**
   * 
   * @param {JSON} credentials 
   * credentials example: 
   * {"user":"userExample", "host":"hostExample", "password":"passwordExample", "database":"databaseExample", "port":"portExample", "ssl":true}
   * @param {String} table 
   * @param {Array<String>} fields 
   * @param {Array<JSON>} clauses clauses example: [{ 'id_company': '001'}, {'client_name': 'Zezinho'}] 
   * @param {String} operator AND or OR 
   */
  async update(credentials, table, fields, clauses, operator) {
      const where = GenericHelper.transformJsonToFormatedString(clauses, operator);
      const fieldUpdate = GenericHelper.transformJsonToFormatedString(fields, ",");
      const query = `UPDATE ${table} SET ${fieldUpdate} WHERE ${where}`;
      return await this.executeQuery(query, credentials);
  }

 /**
  * Delete with Where
  * @param {JSON} credentials 
  * credentials example: 
  * {"user":"userExample", "host":"hostExample", "password":"passwordExample", "database":"databaseExample", "port":"portExample", "ssl":true}
  * @param {String} table 
  * @param {Array<JSON>} clauses clauses example: [{ 'id_company': '001'}, {'client_name': 'Zezinho'}] 
  * @param {String} operator AND or OR  
  */
  async delete(credentials, table, clauses, operator) {
      const where  = GenericHelper.transformJsonToFormatedString(clauses, operator);
      const query = `DELETE FROM ${table} WHERE ${where}`;
      return await this.executeQuery(query, credentials);
  }
}

module.exports = new DatabaseService();

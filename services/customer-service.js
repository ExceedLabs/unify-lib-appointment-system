const Tables = require('../const/tables');

class CustomerService {
    static mountCustomerQueries (method, companyId, customerId) {
        const tableName = Tables.TABLE_NAMES.CUSTOMER;
        const tableSchema = Tables.TABLE_SCHEMAS.CUSTOMER;
        let clause;
        const fieldsToRetrive = Object.keys(tableSchema).map( key => { 
            return tableSchema[key].name 
            }
        );
        switch(method) {
            case 'GET_BY_COMPANY':
                clause =  { [tableSchema.company_id.name]: companyId };
                return { fieldsToRetrive, clause, tableName };
            case 'GET':
                clause = { [tableSchema.id.name]: customerId };
                return { fieldsToRetrive, clause, tableName };
            default:
                throw(`Method empty or not recognized! Method: ${method}`);
        }
    }
}
module.exports = CustomerService;
const GenericHelper = require('../helpers/generic-helper.js');
const Tables = require('../const/tables');
const moment = require('moment');

const tableName = Tables.TABLE_NAMES.APPOINTMENT;
const tableSchema = Tables.TABLE_SCHEMAS.APPOINTMENT;

class AppointmentService {
    static generateOnenessSubquery (mainTableAlias, subTableAlias) {
        if(mainTableAlias && subTableAlias && (mainTableAlias !== subTableAlias)) {
            return `SELECT MAX(${subTableAlias}.${tableSchema.update_at.name}) FROM `
            + `${tableName} as ${subTableAlias} WHERE ${subTableAlias}.${tableSchema.appointment_id.name} = `
            + `${mainTableAlias}.${tableSchema.appointment_id.name}`
        } else {
            throw(`You must provide different valid aliases! Aliases provided ${mainTableAlias}, ${subTableAlias}`);
        }
    }

    static mountBaseSelectRangeQuery (starts, ends) {
        const mainTableAlias = 'ap1';
        const subQueryTableAlias = 'ap2';
        const tableFields = Object.keys(tableSchema).map( key => `${mainTableAlias}.${tableSchema[key].name}` );
        const baseSelect = `SELECT ${tableFields.reduce((elem, acc) => `${acc}, ${elem}`)} FROM ${tableName} AS ${mainTableAlias}`;
        const onenessSubquery = this.generateOnenessSubquery(mainTableAlias, subQueryTableAlias);
        const whereClause = `WHERE (${mainTableAlias}.${tableSchema.starts.name} >= '${starts}' ` 
            + `AND ${mainTableAlias}.${tableSchema.ends.name} <= '${ends}') AND `
            + `${mainTableAlias}.${tableSchema.update_at.name} = (${onenessSubquery})`
        return `${baseSelect} ${whereClause}`;
    }

    static mountFinalQueryWithParams (baseQuery, params, operator) {
        if(operator === 'AND' || operator === 'OR') {
            const validParams = Object.keys(params).filter( paramKey => { 
                const isValid = Object.keys(tableSchema).find( tableKey => tableSchema[tableKey].name === paramKey);
                const hasValidValue = params[paramKey] !== '' && params[paramKey] !== undefined && paramKey[paramKey] !== null;
                if(isValid && hasValidValue) {
                    return true;
                }
                return false;
            });
            const whereClauses = validParams.map( paramKey => {
                const tableSchemaKey = Object.keys(tableSchema).find( elem => tableSchema[elem].name === paramKey);
                return { [tableSchema[tableSchemaKey].name] : params[paramKey] }
            });
            const finalClausesObj = GenericHelper.reduceObjectArray(whereClauses);
            const whereQuery = `(${GenericHelper.transformJsonToFormatedString(finalClausesObj, operator)})`;
            return `${baseQuery} AND ${whereQuery}`;
        } else {
            throw(`Provide a valid operator! Operator provided ${operator}`);
        }
    }
}
module.exports = AppointmentService;

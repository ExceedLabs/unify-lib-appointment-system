const CompanyModel = require('./models/company-model.js');
const StoreModel = require('./models/store-model.js');
const AppointmentTypesModel = require('./models/appointment-type-model.js');
const EmployeeModel = require('./models/employee-model.js');
const SlotModel = require('./models/slot-model.js');
const AppointmentModel = require('./models/appointment-model.js');
const CustomerModel = require('./models/customer-model.js');

exports.getCompanyById = CompanyModel.getCompanyById;
exports.getStoresByCompanyId = StoreModel.getStoresByCompanyId;
exports.getAppointmentTypesByStoreId = AppointmentTypesModel.getAppointmentTypesByStoreId;
exports.getEmployeesByStoreId = EmployeeModel.getEmployeesByStoreId;
exports.getSlotsByStoreId = SlotModel.getSlotsByStoreId;
exports.getAppointmentsByStoreId = AppointmentModel.getAppointmentsByStoreId;
exports.postAppointment = AppointmentModel.postAppointment;
exports.getCustomersByCompanyId = CustomerModel.getCustomersByCompanyId;
exports.getCustomerById = CustomerModel.getCustomerById;
exports.updateAppointment = AppointmentModel.updateAppointment;

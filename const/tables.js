
module.exports = {
    TABLE_NAMES: {
        COMPANY:"company",
        STORE:"store",
        APPOINTMENT_TYPE:"appointment_type",
        APPOINTMENT_TYPE_STORE:"appointment_type_store",
        SLOTS:"slots",
        SLOTS_APPOINTMENT_TYPES:"slots_appointment_types",
        ROLES:"roles",
        ROLES_APPOINTMENT_TYPES:"roles_appointment_types",
        EMPLOYEE:"employee",
        CUSTOMER:"customer",
        APPOINTMENT:"appointment"
    },
    TABLE_SCHEMAS: {
        COMPANY: {
            id: {
                name: 'id',
                type: 'String'
            },
            company_id: {
                name: 'company_id',
                type: 'String'
            },
            name: {
                name: 'name',
                type: 'String'
            },
            description: {
                name: 'description',
                type: 'String'
            },
            active: {
                name: 'active',
                type: 'Boolean'
            }
        },
        STORE: {
            id: {
                name: 'id',
                type: 'String'
            },
            company_id: {
                name: 'company_id',
                type: 'Integer'
            },
            name: {
                name: 'name',
                type: 'String'
            },    
            street: {
                name: 'street',
                type: 'String'
            },
            complement: {
                name: 'complement',
                type: 'String'
            },
            country: {
                name: 'country',
                type: 'String'
            }, 
            city: {
                name: 'city',
                type: 'String'
            }, 
            state: {
                name: 'state',
                type: 'String'
            },
            lat: {
                name: 'lat',
                type: 'Double'
            },
            lng: {
                name: 'lng',
                type: 'Double'
            },
            active: {
                name: 'active',
                type: 'Boolean'
            },
        },
        APPOINTMENT_TYPE: {
            id: {
                name: 'id',
                type: 'Integer'
            },
            company_id: {
                name: 'company_id',
                type: 'Integer'
            },
            name: {
                name: 'name',
                type: 'String'
            },
            description: {
                name: 'description',
                type: 'String'
            },
            active: {
                name: 'active',
                type: 'Boolean'
            },
            type: {
                name: 'type',
                type: 'String'
            },
        },
        APPOINTMENT_TYPE_STORE: {
            id: {
                name: 'id',
                type: 'Integer'
            },
            appointment_type_id: {
                name: 'appointment_type_id',
                type: 'Integer'
            },
            store_id: {
                name: 'store_id',
                type: 'Integer'
            }
        },
        SLOT: {
            id: {
                name: 'id',
                type: 'Integer'
            },
            store_id: {
                name: 'store_id',
                type: 'Integer'
            },
            name: {
                name: 'name',
                type: 'String'             
            },
            people_qty: {
                name: 'people_qty',
                type: 'Integer'   
            },
            timeslice: {
                name: 'timeslice',
                type: 'Integer'  
            },
            active: {
                name: 'active',
                type: 'Boolean' 
            },
            opens: {
                name: 'opens',
                type: 'Datetime' 
            },
            closes: {
                name: 'closes',
                type: 'Datetime' 
            },
        },
        SLOTS_APPOINTMENT_TYPES: {
            id: {
                name: 'id',
                type: 'Integer'
            },
            appointment_type_id: {
                name: 'appointment_type_id',
                type: 'Integer'
            },
            slot_id: {
                name: 'slot_id',
                type: 'Integer'
            }
        },
        EMPLOYEE: {
            id: {
                name: 'id',
                type: 'Integer'
            },
            store_id: {
                name: 'store_id',
                type: 'Integer'
            },   
            first_name: {
                name: 'first_name',
                type: 'String'
            }, 
            middle_name: {
                name: 'middle_name',
                type: 'String'
            }, 
            last_name: {
                name: 'last_name',
                type: 'String'
            }, 
            role: {
                name: 'role',
                type: 'Integer'
            }, 
            active: {
                name: 'active',
                type: 'Boolean'
            },
        },
        ROLES: {
            id: {
                name: 'id',
                type: 'Integer'
            },
            company_id: {
                name: 'company_id',
                type: 'Integer'
            },
            name: {
                name: 'name',
                type: 'String'
            },
            description: {
                name: 'description',
                type: 'String'
            },
        },
        ROLES_APPOINTMENT_TYPES: {
            id: {
                name: 'id',
                type: 'Integer'
            },
            appointment_type_id: {
                name: 'appointment_type_id',
                type: 'Integer'
            },
            role_id: {
                name: 'role_id',
                type: 'Integer'
            }
        },
        APPOINTMENT: {
            id: {
                name: 'id',
                type: 'Integer'
            },
            appointment_id: {
                name: 'appointment_id',
                type: 'Integer'
            },
            employee_id: {
                name: 'employee_id',
                type: 'Integer'
            },
            appointment_type_id: {
                name: 'appointment_type_id',
                type: 'Integer'
            },
            slot_id: {
                name: 'slot_id',
                type: 'Integer'
            },
            customer_id: {
                name: 'customer_id',
                type: 'Integer'
            },
            starts: {
                name: 'starts',
                type: 'Datetime'
            },
            ends: {
                name: 'ends',
                type: 'Datetime'
            },
            update_at: {
                name: 'update_at',
                type: 'Datetime'
            },
            status: {
                name: 'status',
                type: 'String'
            },
            description: {
                name: 'description',
                type: 'String'
            },
            comments: {
                name: 'comments',
                type: 'String'
            },
            store_id: {
                name: 'store_id',
                type: 'String'
            },
        },
        CUSTOMER: {
            id: {
                name: 'id',
                type: 'Integer'
            },
            company_id: {
                name: 'company_id',
                type: 'Integer'
            },
            email: {
                name: 'email',
                type: 'String'
            },
            first_name: {
                name: 'first_name',
                type: 'String'
            },
            middle_name: {
                name: 'middle_name',
                type: 'String'
            },
            last_name: {
                name: 'last_name',
                type: 'String'
            },
            phone: {
                name: 'phone',
                type: 'String'
            },
            gender: {
                name: 'gender',
                type: 'String'
            },
        }
    }
};

class GenericHelper {
  static retrieveCoreObjects (coreObj) {
      const { req } = coreObj;
      const { MessageService, Converters } = coreObj.privateLibraries;
      const intLibraryConf = coreObj.infoOrganization.integrationLibs.find( elem => elem.id === 'unify-lib-appointment-system');
      return { req, MessageService, Converters, intLibraryConf }
  }

  /**
  * Transform the json in a string with the follwing format of output
      Input: {key1 : value1, key2, value2}, operator = 'AND'
      Output: "key1 = value1 AND key2 = key2"
  * @param {Array<JSON>} value 
  * @param {String} separator 
  */
  static transformJsonToFormatedString (json, separator) {
    const auxFunc = (jsonKeys, acc) => {
        if(jsonKeys.length > 1) {
            const key = jsonKeys.shift();
            const newAcc = `${acc}${key} = ${json[key]} ${separator} `;
            return auxFunc(jsonKeys, newAcc);
          } else if (jsonKeys.length == 1) {
            const key = jsonKeys.shift();
            const newAcc = `${acc}${key} = ${json[key]}`;
            return auxFunc(jsonKeys, newAcc);
          } else {
            return acc;
          }         
    }
    if (Object.keys(json).length > 1) {
          if(separator) {
            return auxFunc(Object.keys(json), "");
          }
          throw "Invalid separator";
    }
    return auxFunc(Object.keys(json), "");   
  }

  /**
  * Get the keys and values of an array of json with just one level
  * @param {Array<JSON>} jsonArray 
  */
  static jsonGetKeysValues (jsonArray){        
    function auxFunc(jsonArray, keyValue) {
      if (jsonArray.length >= 1){
        const json = jsonArray.shift();
        const keyField = Object.keys(json)[0];
        const valueField = Object.values(json)[0];
        keyValue[keyField] = valueField;    
        return auxFunc(jsonArray, keyValue);
      } else {
        return keyValue;
      }
    };
        
    if (jsonArray.length >= 1){
      return auxFunc(jsonArray, {});
    } else {
      throw `Invalid value parameter, lengh: ${value.length}`; 
    }
  }

  /**
   * Convert the camelCases Keys in underscore
   * @param {JSON} json 
   */
  static jsonKeysCamelCaseToUnderscore (json) {
    const keys = Object.keys(json);
    keys.forEach(elem => {
        if (/(?=[A-Z])/.test(elem)){
            json[elem.split(/(?=[A-Z])/).join('_').toLowerCase()] = json[elem];
            delete json[elem];
        }        
    })
    return json;
  }

  static jsonKeysUnderscoreToCamelCase (json = {}) {
    const keys = Object.keys(json);
    keys.forEach( elem => {
      const splitedElements = elem.split('_');
      if(splitedElements.length > 1) {
        const newKey = splitedElements.reduce((acc, cur) => acc + (cur.charAt(0).toUpperCase() + cur.slice(1)))
        json[newKey] = json[elem];
        delete json[elem];
      }
    });
    return json;
  }

  static reduceObjectArray(objectArray = [], result ={} ) {
    if(objectArray.length === 0) {
      return result
    }
    const [ head, ...tail ] = objectArray;
    return this.reduceObjectArray(tail, { ...result, ...head } );
  }
}

module.exports = GenericHelper;

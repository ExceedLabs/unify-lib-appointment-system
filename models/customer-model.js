const GenericHelper = require('../helpers/generic-helper');
const DatabaseService = require('../services/database-service');
const CustomerService = require('../services/customer-service');

class CustomerModel {
    static async getCustomersByCompanyId(coreObj) {
        const { req, MessageService, Converters, intLibraryConf } = GenericHelper.retrieveCoreObjects(coreObj);
        const companyId = req.params.company_id;
        if(companyId) {
            try {
                const queryObjs = CustomerService.mountCustomerQueries('GET_BY_COMPANY', companyId);
                const result = await DatabaseService.select(
                    intLibraryConf.credentials,
                    queryObjs.tableName,
                    queryObjs.fieldsToRetrive,
                    queryObjs.clause
                );
                return MessageService.getSuccess(
                    result.rows.map( elem => GenericHelper.jsonKeysUnderscoreToCamelCase(elem)),
                    200,
                    'OK'
                  );
            } catch (e) {
                return MessageService.getError('Error executing DB query', 500, 'An error ocurred on retrieving values from the DB');
            }
        } else {
            return MessageService.getError('Empty Company ID', 400, 'You must provide a company ID!');
        }
    }

    static async getCustomerById(coreObj) {
        const { req, MessageService, Converters, intLibraryConf } = GenericHelper.retrieveCoreObjects(coreObj);
        const companyId = req.params.company_id;
        const customerId = req.params.customer_id;
        if(companyId && customerId) {
            try {
                const queryObjs = CustomerService.mountCustomerQueries('GET', companyId, customerId);
                const result = await DatabaseService.select(
                    intLibraryConf.credentials,
                    queryObjs.tableName,
                    queryObjs.fieldsToRetrive,
                    queryObjs.clause
                );
                return MessageService.getSuccess(
                    GenericHelper.jsonKeysUnderscoreToCamelCase(result.rows[0]),
                    200,
                    'OK'
                  );
            } catch (e) {
                return MessageService.getError('Error executing DB query', 500, 'An error ocurred on retrieving values from the DB');
            }
        } else {
            if(customerId) {
                return MessageService.getError('Empty Company ID', 400, 'You must provide a company ID!');
            } else {
                return MessageService.getError('Empty Customer ID', 400, 'You must provide a customer ID!');
            }
        }
    }
}
module.exports = CustomerModel;
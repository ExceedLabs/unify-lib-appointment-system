const GenericHelper = require('../helpers/generic-helper.js');
const DatabaseService = require('../services/database-service.js');
const TablesConstants = require('../const/tables.js');

const mountStoreQueriesObjs = (companyId, method) => {
    const tableName = TablesConstants.TABLE_NAMES.STORE;
    const tableSchema = TablesConstants.TABLE_SCHEMAS.STORE;
    switch(method) {
      case 'GET':
        const fieldsToRetrive = Object.keys(tableSchema).map( key => { 
          return tableSchema[key].name 
          }
        );
        const clause = { [tableSchema.company_id.name]: `'${companyId}'` };
        return { fieldsToRetrive, clause, tableName }
      default:
        throw(`Method empty or not recognized! Method: ${method}`);
    }
};

class StoreModel {
    static async getStoresByCompanyId(coreObj) {
        const { req, MessageService, Converters, intLibraryConf } = GenericHelper.retrieveCoreObjects(coreObj);
        const companyId = req.params.company_id;
        if(companyId) {
            const queryObjs = mountStoreQueriesObjs(companyId, 'GET');
            try {
                const result = await DatabaseService.select(
                    intLibraryConf.credentials,
                    queryObjs.tableName,
                    queryObjs.fieldsToRetrive,
                    queryObjs.clause
                  );
                  return MessageService.getSuccess(
                    result.rows.map( elem => GenericHelper.jsonKeysUnderscoreToCamelCase(elem)),
                    200,
                    'OK'
                  );
            } catch (e) {
                return MessageService.getError('Error executing DB query', 500, 'An error ocurred on retrieving values from the DB');
            }
        } else {
            if(companyId) {
                return MessageService.getError('Empty Store ID', 400, 'You must provide a Store ID!');
            }
            return MessageService.getError('Empty Company ID', 400, 'You must provide a company ID!');
        }
    }
}
module.exports = StoreModel;

const GenericHelper = require('../helpers/generic-helper.js');
const DatabaseService = require('../services/database-service.js');
const TablesConstants = require('../const/tables.js');

const mountStoreQueriesObjs = (method, companyId, storeId ) => {
  const tableName = TablesConstants.TABLE_NAMES.APPOINTMENT_TYPE;
  const tableSchema = TablesConstants.TABLE_SCHEMAS.APPOINTMENT_TYPE;
  switch(method) {
    case 'GET_BY_STORE':
      const relationshipTableName = TablesConstants.TABLE_NAMES.APPOINTMENT_TYPE_STORE;
      const relationshipTableSchema = TablesConstants.TABLE_SCHEMAS.APPOINTMENT_TYPE_STORE;
      const storeTableName = TablesConstants.TABLE_NAMES.STORE;
      const storeTableSchema = TablesConstants.TABLE_SCHEMAS.STORE;
      const innerJoinClause = `INNER JOIN ${relationshipTableName} ON ` + 
      `${tableName}.${tableSchema.id.name} = ${relationshipTableName}.${relationshipTableSchema.appointment_type_id.name} `
        + `INNER JOIN ${storeTableName} ON ${relationshipTableName}.${relationshipTableSchema.store_id.name} = ${storeTableName}.${storeTableSchema.id.name}`;
      const fieldsToRetrive = Object.keys(tableSchema).map( key => `${tableName}.${tableSchema[key].name}`);
      const selectClause = `SELECT ${fieldsToRetrive.reduce((elem, acc) => `${acc}, ${elem}`)} FROM ${tableName}`
      const whereConditions = {
        [`${tableName}.${tableSchema.company_id.name}`]: companyId,
        [`${storeTableName}.${storeTableSchema.id.name}`]: storeId
      }; 
      const whereClause = GenericHelper.transformJsonToFormatedString(whereConditions, 'AND');
      return `${selectClause} ${innerJoinClause} WHERE ${whereClause}`;
    default:
      throw(`Method empty or not recognized! Method: ${method}`);
  }
};

class AppointmentTypeModel {
  static async getAppointmentTypesByStoreId(coreObj) {
      const { req, MessageService, Converters, intLibraryConf } = GenericHelper.retrieveCoreObjects(coreObj);
      const companyId = req.params.company_id;
      const storeId = req.params.store_id;
      if(companyId && storeId) {
        const fullQuery = mountStoreQueriesObjs('GET_BY_STORE', companyId, storeId);
        try {
          const result = await DatabaseService.executeQuery(
            intLibraryConf.credentials,
            fullQuery
          );
          return MessageService.getSuccess(
            result.rows.map( elem => GenericHelper.jsonKeysUnderscoreToCamelCase(elem)),
            200,
            'OK'
          );
        } catch (e) {
          return MessageService.getError('Error executing DB query', 500, 'An error ocurred on retrieving values from the DB');
        }
     } else {
        if(companyId) {
            return MessageService.getError('Empty Store ID', 400, 'You must provide a Store ID!');
        }
        return MessageService.getError('Empty Company ID', 400, 'You must provide a company ID!');
    }
  }
}
module.exports = AppointmentTypeModel;
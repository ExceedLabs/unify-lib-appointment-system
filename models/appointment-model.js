const GenericHelper = require('../helpers/generic-helper');
const DatabaseService = require('../services/database-service');
const AppointmentService = require('../services/appointment-service');
const Tables = require('../const/tables');
const moment = require('moment');

const mountAppointmentQueriesObjs = (method, storeId, params) => {
  const { starts, ends, employee_id, slot_id } = params;
  switch(method) {
    case 'GET_BY_STORE':
      if(starts && ends) {
        const startDate = moment(starts);
        const endDate = moment(ends);
        if(startDate.isValid() && endDate.isValid()) {
          const baseQuery = AppointmentService.mountBaseSelectRangeQuery(starts, ends);
          const fullQuery = AppointmentService.mountFinalQueryWithParams(
            baseQuery, 
            { 'store_id': storeId },
            'AND'
          );
          if(employee_id || slot_id) {
            return AppointmentService.mountFinalQueryWithParams(fullQuery, {employee_id, slot_id}, 'OR');
          }
          return fullQuery;
        }
      }
      throw('Invalid Rage! You must provide a valid range');
    default:
      throw(`Method empty or not recognized! Method: ${method}`);
  }
};

class AppointmentModel {
  static async getAppointmentsByStoreId(coreObj) {
    const { req, MessageService, Converters, intLibraryConf } = GenericHelper.retrieveCoreObjects(coreObj);
    const companyId = req.params.company_id;
    const storeId = req.params.store_id;
    const queryParams = req.query;
    if(companyId && storeId) {
      if(Object.keys(queryParams).length) {
        const { starts, ends } = queryParams;
        if(starts && ends) {
          try {
            const query = mountAppointmentQueriesObjs('GET_BY_STORE', storeId, queryParams);
            const result = await DatabaseService.executeQuery(
              intLibraryConf.credentials,
              query
            );
            return MessageService.getSuccess(
              result.rows.map( row => GenericHelper.jsonKeysUnderscoreToCamelCase(row) ),
              200,
              'OK'
            );
          } catch (e) {
            return MessageService.getError('Error executing DB query', 500, 'An error ocurred on retrieving values from the DB');
          }
        }
        return MessageService.getError('Empty range', 400, 'You must provide the date range where you want to retrieve the appointments!');
      } else {
        return MessageService.getError('Empty range', 400, 'You must provide the date range where you want to retrieve the appointments!');
      }
    } else {
      if(companyId) {
        return MessageService.getError('Empty Store ID', 400, 'You must provide a Store ID!');
      }
      return MessageService.getError('Empty Company ID', 400, 'You must provide a company ID!');
    }
  }

  static async postAppointment(coreObj) {
    const { req, MessageService, Converters, intLibraryConf } = GenericHelper.retrieveCoreObjects(coreObj);
    const appointmentTable = Tables.TABLE_NAMES.APPOINTMENT;
    const companyId = req.params.company_id;
    const storeId = req.params.store_id;
    const hasBody = req.body;
    if (companyId && storeId) {
      if (hasBody) {
        const bodyParams = GenericHelper.jsonKeysCamelCaseToUnderscore({...hasBody, "storeId": parseInt(storeId)});
        if (req.method === "POST"){
          try {
            /** Get the max ID for the appointment table, increment by 1 add to the clauses */
            const queryGetLastId = `SELECT MAX(appointment_id) FROM ${appointmentTable}`;
            const lastId = await DatabaseService.executeQuery(intLibraryConf.credentials, queryGetLastId);
            const newIdAppointmentId = lastId.rows[0].max+1;
            const newClauses = {...bodyParams, "appointment_id": newIdAppointmentId};
            const data = await DatabaseService.insert(
              intLibraryConf.credentials, 
              appointmentTable, 
              newClauses
            );
            return MessageService.getSuccess({"appointment_id": newIdAppointmentId}, 200, "ok"); 
          } catch (error) {
            return MessageService.getError(error.stack, 400, 'Query Error'); 
          }
        } else if (req.method === "PUT") {
          try {
            if (bodyParams.appointment_id){
              const data = await DatabaseService.insert(
                intLibraryConf.credentials, 
                appointmentTable, 
                bodyParams
              );
              return MessageService.getSuccess(data, 200, "ok");          
            } else {
              return MessageService.getError("Update/PUT without appointmentId", 400, "Update Error");
            }            
         } catch (error) {
           return MessageService.getError(error.stack, 400, 'Query Error'); 
         }
        }
      } else {
        return MessageService.getError('Invalid body/json', 400, 'You must provide a valid Json with values to insert an appointment'); 
      }
      
    }
  }  
}
module.exports = AppointmentModel;

const GenericHelper = require('../helpers/generic-helper.js');
const DatabaseService = require('../services/database-service.js');
const TablesConstants = require('../const/tables.js');

const mountCompanyQueriesObjs = (companyId, method) => {
  const tableName = TablesConstants.TABLE_NAMES.COMPANY;
  const tableSchema = TablesConstants.TABLE_SCHEMAS.COMPANY;
  switch(method) {
    case 'GET':
      const fieldsToRetrive = Object.keys(tableSchema).map( key => { 
        return tableSchema[key].name 
        }
      );
      const clause = { [tableSchema.company_id.name]: `'${companyId}'` };
      return { fieldsToRetrive, clause, tableName }
    default:
      throw(`Method empty or not recognized! Method: ${method}`);
  }
};

class CompanyModel {
  static async getCompanyById(coreObj) {
    const { req, MessageService, Converters, intLibraryConf } = GenericHelper.retrieveCoreObjects(coreObj);
    const companyId = req.params.company_id;
    if(companyId) {
      const queryObjs = mountCompanyQueriesObjs(companyId, 'GET');
      try {
        const result = await DatabaseService.select(
          intLibraryConf.credentials,
          queryObjs.tableName,
          queryObjs.fieldsToRetrive,
          queryObjs.clause
        );
        return MessageService.getSuccess(
          GenericHelper.jsonKeysUnderscoreToCamelCase(result.rows[0]),
          200,
          'OK'
        );
      } catch (e) {
        return MessageService.getError('Error executing DB query', 500, 'An error ocurred on retrieving values from the DB');
      }
    } else {
      return MessageService.getError('Empty Company ID', 400, 'You must provide a company ID!');
    }
  }
}

module.exports = CompanyModel;

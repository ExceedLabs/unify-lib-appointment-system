const GenericHelper = require('../helpers/generic-helper.js');
const DatabaseService = require('../services/database-service.js');
const TablesConstants = require('../const/tables.js');

const mountEmployeeQueriesObjs = (method, storeId, appointmentTypeId ) => {
  const tableName = TablesConstants.TABLE_NAMES.EMPLOYEE;
  const tableSchema = TablesConstants.TABLE_SCHEMAS.EMPLOYEE;
  let fieldsToRetrive = [];
  switch(method) {
    case 'GET_BY_APPOINTMENT_TYPE':
      const relationshipTableName = TablesConstants.TABLE_NAMES.ROLES_APPOINTMENT_TYPES;
      const relationshipTableSchema = TablesConstants.TABLE_SCHEMAS.ROLES_APPOINTMENT_TYPES;
      const appTypeTableName = TablesConstants.TABLE_NAMES.APPOINTMENT_TYPE;
      const appTypeTableSchema = TablesConstants.TABLE_SCHEMAS.APPOINTMENT_TYPE;
      const innerJoinClause = `INNER JOIN ${relationshipTableName} ON ${tableName}.${tableSchema.role.name} = ` + 
        `${relationshipTableName}.${relationshipTableSchema.role_id.name} ` +
        `INNER JOIN ${appTypeTableName} ON ${relationshipTableName}.${relationshipTableSchema.appointment_type_id.name} = ` + 
        `${appTypeTableName}.${appTypeTableSchema.id.name}`;
      fieldsToRetrive = Object.keys(tableSchema).map( key => `${tableName}.${tableSchema[key].name}`);
      const selectClause = `SELECT ${fieldsToRetrive.reduce((elem, acc) => `${acc}, ${elem}`)} FROM ${tableName}`
      const whereConditions = {
        [`${tableName}.${tableSchema.store_id.name}`]: storeId,
        [`${appTypeTableName}.${appTypeTableSchema.id.name}`]: appointmentTypeId 
      };
      const whereClause = GenericHelper.transformJsonToFormatedString(whereConditions, 'AND');
      return `${selectClause} ${innerJoinClause} WHERE ${whereClause}`;
    case 'GET_BY_STORE':
      fieldsToRetrive = Object.keys(tableSchema).map( key => { 
        return tableSchema[key].name 
        }
      );
      const clause = { [tableSchema.store_id.name]: storeId };
      return { fieldsToRetrive, clause, tableName }
    default:
      throw(`Method empty or not recognized! Method: ${method}`);
  }
};

class EmployeeModel {
  static async getEmployeesByStoreId(coreObj) {
    const { req, MessageService, Converters, intLibraryConf } = GenericHelper.retrieveCoreObjects(coreObj);
    const companyId = req.params.company_id;
    const storeId = req.params.store_id;
    const hasQueryParams = req.query;
    if(companyId && storeId) {
      if(Object.keys(hasQueryParams).length) {
        if(hasQueryParams.appointment_type) {
          const fullQuery = mountEmployeeQueriesObjs('GET_BY_APPOINTMENT_TYPE', storeId, hasQueryParams.appointment_type);
          try {
            const result = await DatabaseService.executeQuery(
              intLibraryConf.credentials,
              fullQuery
            );
            return MessageService.getSuccess(
              result.rows.map( elem => GenericHelper.jsonKeysUnderscoreToCamelCase(elem)),
              200,
              'OK'
            );
          } catch (e) {
            return MessageService.getError('Error executing DB query', 500, 'An error ocurred on retrieving values from the DB');
          }
        } else {
          return MessageService.getError('Unsupported Query Param', 400, `The query param(s) ${hasQueryParams} is/are not supported by the system.`);
        }
      } else {
        const queryObjs = mountEmployeeQueriesObjs('GET_BY_STORE', storeId);
        try {
          const result = await DatabaseService.select(
            intLibraryConf.credentials,
            queryObjs.tableName,
            queryObjs.fieldsToRetrive,
            queryObjs.clause
          );
          return MessageService.getSuccess(
            result.rows,
            200,
            'OK'
          )
        } catch (e) {
          return MessageService.getError('Error executing DB query', 500, 'An error ocurred on retrieving values from the DB');
        }
      }
    } else {
      if(companyId) {
        return MessageService.getError('Empty Store ID', 400, 'You must provide a Store ID!');
      }
      return MessageService.getError('Empty Company ID', 400, 'You must provide a company ID!');
      }
  }
}
module.exports = EmployeeModel;